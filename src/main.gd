extends Control

@onready var grid = $VBoxContainer/GridContainer
@onready var hud = $VBoxContainer/HUD
var plate_btn = preload("res://plate.tscn")

var plates = [
	"res://plates/USA/output-1718115815-1.jpg",
  "res://plates/USA/output-1718115815-5.jpg",
  "res://plates/USA/output-1718115815-11.jpg",
  "res://plates/USA/output-1718115815-15.jpg",
  "res://plates/USA/output-1718115815-17.jpg",
  "res://plates/USA/output-1718115864-1.jpg",
  "res://plates/USA/output-1718115864-3.jpg",
  "res://plates/USA/output-1718115864-9.jpg",
  "res://plates/USA/output-1718115864-11.jpg",
  "res://plates/USA/output-1718115864-13.jpg",
  "res://plates/USA/output-1718115888-3.jpg",
  "res://plates/USA/output-1718115888-5.jpg",
  "res://plates/USA/output-1718115888-7.jpg",
  "res://plates/USA/output-1718115888-11.jpg",
  "res://plates/USA/output-1718115888-13.jpg",
  "res://plates/USA/output-1718115924-1.jpg",
  "res://plates/USA/output-1718115924-3.jpg",
  "res://plates/USA/output-1718115924-7.jpg",
  "res://plates/USA/output-1718115924-9.jpg",
  "res://plates/USA/output-1718115924-11.jpg",
  "res://plates/USA/output-1718115970-1.jpg",
  "res://plates/USA/output-1718115970-3.jpg",
  "res://plates/USA/output-1718115970-5.jpg",
  "res://plates/USA/output-1718115970-7.jpg",
  "res://plates/USA/output-1718115970-9.jpg",
  "res://plates/USA/output-1718116041-1.jpg",
  "res://plates/USA/output-1718116041-3.jpg",
  "res://plates/USA/output-1718116041-5.jpg",
  "res://plates/USA/output-1718116041-7.jpg",
  "res://plates/USA/output-1718116041-11.jpg",
  "res://plates/USA/output-1718116102-1.jpg",
  "res://plates/USA/output-1718116102-3.jpg",
  "res://plates/USA/output-1718116102-5.jpg",
  "res://plates/USA/output-1718116102-7.jpg",
  "res://plates/USA/output-1718116102-9.jpg",
  "res://plates/USA/output-1718116174-3.jpg",
  "res://plates/USA/output-1718116174-5.jpg",
  "res://plates/USA/output-1718116174-7.jpg",
  "res://plates/USA/output-1718116174-11.jpg",
  "res://plates/USA/output-1718116174-13.jpg",
  "res://plates/USA/output-1718116211-1.jpg",
  "res://plates/USA/output-1718116211-5.jpg",
  "res://plates/USA/output-1718116211-9.jpg",
  "res://plates/USA/output-1718116211-13.jpg",
  "res://plates/USA/output-1718116211-17.jpg",
  "res://plates/USA/output-1718116235-1.jpg",
  "res://plates/USA/output-1718116235-3.jpg",
  "res://plates/USA/output-1718116235-5.jpg",
  "res://plates/USA/output-1718116235-9.jpg",
  "res://plates/USA/output-1718116235-11.jpg"
]

# Called when the node enters the scene tree for the first time.
func _ready() -> void:
	for plate in plates:
		var img = ResourceLoader.load(plate)
		var btn = plate_btn.instantiate()
		btn.icon = img
		grid.add_child(btn)

# Called every frame. 'delta' is the elapsed time since the previous frame.
func _process(delta: float) -> void:
	var found = 0
	var plates = get_tree().get_nodes_in_group("plates")
	for p in plates:
		if p.button_pressed:
			found = found + 1 
	hud.text = "Plates Found: " + str(found) + " of " + str(plates.size())
